# test-project

<div style="display: flex; flex-direction: row; align-items: center;">

<a href="https://gitlab.com/stack7013932/python/test-project">
    <img alt="pipeline status" src="https://gitlab.com/stack7013932/python/test-project/badges/main/pipeline.svg">
</a>
<span style="display:inline-block; width: 1em;"></span>
<a href="https://gitlab.com/stack7013932/python/test-project/-/releases">
    <img alt="latest release" src="https://gitlab.com/stack7013932/python/test-project/-/badges/release.svg?order_by=release_at">
</a>
<span style="display:inline-block; width: 1em;"></span>
<a href="https://gitlab.com/stack7013932/python/test-project/">
    <img alt="latest release" src="https://gitlab.com/stack7013932/python/test-project/badges/main/coverage.svg">
</a>
<span style="display:inline-block; width: 1em;"></span>
<a href="https://github.com/pypa/hatch">
    <img alt="hatch" src="https://img.shields.io/badge/%F0%9F%A5%9A-Hatch-4051b5.svg">
</a>
<span style="display:inline-block; width: 1em;"></span>
<a href="https://github.com/psf/black">
    <img alt="black" src="https://img.shields.io/badge/code%20style-black-000000.svg">
</a>
</div>

Testing the template

## Links

- Documentation : https://stack7013932.gitlab.io/python/test-project
- Issues : https://gitlab.com/stack7013932/python/test-project/-/issues
